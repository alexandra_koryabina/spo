import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {
    ArrayList<TokenMask> maskList;
    TokenValue currentToken;
    LinkedList<TokenValue> tokenList;

    // ����������� �������
    Lexer() {
        tokenList = new LinkedList<TokenValue>();
        maskList = new ArrayList<TokenMask>();
        maskList.add(new TokenMask("help","help",4));
        maskList.add(new TokenMask("com","[a-z]{1,}[A-Za-z]{0,}",4));
        maskList.add(new TokenMask("error","[A-Za-z]{1,}[0-9]{1,}",4));
        maskList.add(new TokenMask("int","0",0));
        maskList.add(new TokenMask("error","[0]{1,}[0-9]{1,}",4));
        maskList.add(new TokenMask("error","[0-9]{11,}",4));
        maskList.add(new TokenMask("int","[1-9]{1}[0-9]{0,9}",0));
        maskList.add(new TokenMask("add","[+]{1}",1));
        maskList.add(new TokenMask("sub","[-]{1}",1));
        maskList.add(new TokenMask("mul","[*]{1}",2));
        maskList.add(new TokenMask("div","[/]{1}",2));
        maskList.add(new TokenMask("lp","[(]{1}",3));
        maskList.add(new TokenMask("rp","[)]{1}",3));
    }

    // �������� ������ �� ������������ �����
    private boolean matchToken(String tokenString) {
        for (TokenMask tm : maskList){
            Pattern tokenPattern = Pattern.compile(tm.mask);
            Matcher tokenMatcher = tokenPattern.matcher(tokenString);

            if (tokenMatcher.matches()) {
                if (tm.name.equals("error")) {
                    currentToken = null;
                    return false;
                }
                currentToken = new TokenValue(tm.name, tokenString,tm.lvl);
                return true;
            }
        }
        return false;
    }

    // ������-������
    public LinkedList<TokenValue> read(String inputString) {
        tokenList.clear();
        int tokenLen = 1;
        currentToken = null;

        for(int pos = 1; pos <= inputString.length(); pos++) {
            String tokenString = inputString.substring(pos - tokenLen, pos);

            if (tokenLen == 1 && tokenString.equals(" "))
                continue;

            if (matchToken(tokenString)) {
                tokenLen++;
                if (pos == inputString.length() && currentToken != null)
                    tokenList.add(currentToken);
            }
            else if (currentToken != null) {
                tokenList.add(currentToken);
                tokenLen = 1;
                currentToken = null;
                pos--;
            }
            else {
                System.out.println("\nSyntax error at " + pos + ". Unknown token " +
                        inputString.substring(pos-tokenLen,pos) + ".");
                return null;
            }
        }
        return tokenList;
    }
}
