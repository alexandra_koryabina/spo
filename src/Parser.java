import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Stack;

public class Parser{

// ���������� �������������� ��������
    private TokenValue doMath(String operation,TokenValue intToken_1,TokenValue intToken_2) {
        TokenValue outToken = new TokenValue("int","0",0);

        switch(operation) {
            case "mul":
                outToken.value = Integer.toString(Integer.parseInt(intToken_1.value) * Integer.parseInt(intToken_2.value));
                break;
            case "div":
                if (intToken_2.value.equals("0")) {
                    outToken.name = "error";
                    outToken.value = "You can not divide by zero";
                    return outToken;
                }
                outToken.value = Integer.toString(Integer.parseInt(intToken_1.value) / Integer.parseInt(intToken_2.value));
                break;
            case "add":
                outToken.value = Integer.toString(Integer.parseInt(intToken_1.value) + Integer.parseInt(intToken_2.value));
                break;
            case "sub":
                outToken.value = Integer.toString(Integer.parseInt(intToken_1.value) - Integer.parseInt(intToken_2.value));
                break;
            default:
                outToken.name = "error";
                outToken.value = "Unknown operation";
                return outToken;
        }
        return outToken;
    }

// ���������� ��������
    public String parse(LinkedList<TokenValue> tokenList) {

// ����� ������
        if (tokenList.getFirst().name.equals("help")) {
            if (tokenList.size() != 1)
                return "Syntax error. After 'help'";
            return "Syntax:\n" +
                    "help - display this text\n" +
                    "Expression can contain:\n" +
                    "+ - addition\n" +
                    "- - subtraction\n" +
                    "/ - division\n" +
                    "* - multiplication\n" +
                    "() - brackets.";
        }

// ������ ����� ������� �������
        if (tokenList.getFirst().name.equals("int") || tokenList.getFirst().name.equals("lp"))
            return "Result of the calculation is: " + calculations(tokenList).value;
        else
            return "Syntax error. Incorrect input (first symbol).";
    }

 private TokenValue calculations(LinkedList<TokenValue> tokenList) {

     // �������� �� ������� ��������, ���������� ����������� ��������� (��������� 3 ������)
     for(int i =0; i < tokenList.size();i++) {
         int deleted = 0;
         if(tokenList.get(i).lvl==3) {
             LinkedList<TokenValue> curTokenList = new LinkedList<TokenValue>();
             tokenList.remove(i);
             for(int j = i;j < tokenList.size();j++) {
                 curTokenList.add(tokenList.get(j));
                 tokenList.remove(j);
                 deleted++;
                 if(tokenList.get(j).lvl==3) {
                     tokenList.remove(j);
                     deleted++;
                     break;
                 }
                 j--;
             }
             tokenList.add(i,calculations(curTokenList));
             i--;
         }

     }

     // ���������� �������� ��������� � ������� (��������� 2 ������)
     for(int i =0; i < tokenList.size();i++) {
         if(tokenList.get(i).lvl==2) {
             tokenList.set(i-1,doMath(tokenList.get(i).name,tokenList.get(i-1),tokenList.get(i+1)));
             tokenList.remove(i);
             tokenList.remove(i);
             i-=2;
         }
     }

     // ���������� �������� �������� � ��������� (��������� 1 ������)
     for(int i =0; i < tokenList.size();i++) {
         if (tokenList.get(i).lvl == 1) {
             TokenValue tempToken = doMath(tokenList.get(i).name, tokenList.get(i - 1), tokenList.get(i + 1));
             tokenList.set(i - 1, tempToken);
             tokenList.remove(i);
             tokenList.remove(i);
             i-=2;
         }
     }
     return tokenList.get(0);
 }
}
