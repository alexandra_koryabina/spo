import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;


public class calc {
    public static void main(String[] args) throws IOException {
        System.out.println ("Calc\nEnter your mathematical expression:");
        Lexer lexer = new Lexer();
        Parser parser = new Parser();
        String inputString = new String();
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        LinkedList<TokenValue> tokenList;

        while(!inputString.equals("exit")) {
            System.out.print("> ");
            inputString = stdin.readLine();
            if(inputString == null || inputString.isEmpty() || inputString.equals("exit"))
                continue;

            tokenList = lexer.read(inputString);
            if (tokenList != null) {
                //for (TokenValue tv : tokenList) {
                //System.out.print(tv.name+"("+tv.value+")"+" ");
                //}
                //System.out.println();
                System.out.println(parser.parse(tokenList));
            }
        }

    }
}
